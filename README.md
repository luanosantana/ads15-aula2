
ADS15 Aula 2                                                                                                                             
---                                                                                                                                      
                                                                                                                                         
Códigos desenvolvidos em sala de aula (online), do curso de ADS15 ESR RNP.                                                               
                                                                                                                                         
## Como usar                                                                                                                             
                                                                                                                                         
1. Execute o servidor de log presente na pasta [log-server](./log-server/)                                                               
   ```bash                                                                                                                               
   cd log-server && docker-compose up -d && cd -                                                                                         
   ```                                                                                                                                   
                                                                                                                                         
2. Aguarde o servidor levantar via [localhost](http://localhost:9000) na porta 9000. Faça o login utilizando as credenciais: ``` user=admin, password=admin ```                                                                                                                                                                                                                          
3. Adicione um novo *INPUT* do tipo *UDP GELF* com a opção *global* selecionada, de forma a permitir que o graylog aceite os logs de todos os hosts que enviarem.                                                                                                                       
                                                                                                                                         
4. Execute a aplicação                                                                                                                   
   ```bash                                                                                                                               
   LOG_HOST="seu-ip-local" LOG_PORT="12201" docker-compose up -d                                                                                                                  
   ```
